from student import Student
from course import Course
from tutor import Tutor
from college import College
from record import Record
import time
import datetime
import string

"""
management (summarize its behavior and list the public methods)

handles the main menu system and all the input from the user and subsequent
data cleaning before passing it to the appropriate classes for processing

"""

############################### PRELOADED DATA #################################
#replace with file import if/when possible, and add ability to export to file

college = College("Miss McKeown's School of ASCII Art")
image = r"""    \_/
  --(_)--  .
    / \   /_\
          |Q|
    .-----' '-----.                                  __
   /____[SCHOOL]___\                                ()))
    | [] .-.-. [] |                                (((())
  ..|____|_|_|____|..................................)(..."""

c1 = {'course_number':10001,'course_name':"Introduction to Python",'course_description':"A short course which will teach first programming fundamentals to beginners, using Python.",'course_start':'02-05-19','course_day':"Tues",'course_time':1800,'course_duration':8,'course_cost':0,'course_certified':False,'course_isactive':False,'course_added':'06-08-19','course_addedby':987654}
c2 = {'course_number':10002,'course_name':"Intermediate Python",'course_description':"A short course in more advanced Python topics, for those who already have some programming.",'course_start':'04-05-19','course_day':"Thurs",'course_time':1800,'course_duration':10,'course_cost':10,'course_certified':True,'course_isactive':True,'course_added':'06-08-19','course_addedby':987654}
c3 = {'course_number':10003,'course_name':"Unity for Beginners",'course_description':"Beginning Programming with the Unity Game Engine",'course_start':'23-10-19','course_day':"Wed",'course_time':1900,'course_duration':16,'course_cost':30,'course_certified':True,'course_isactive':True,'course_added':'06-08-19','course_addedby':987654}
course3 = Course(c3)
course1 = Course(c1)
course2 = Course(c2)

t1 = {'staff_number':278459,'first_name':'Kris','last_name':'Jones','pin_number':1234}
t2 = {'staff_number':987654,'first_name':'Ryan','last_name':'Beckett','pin_number':4321}
t3 = {'staff_number':121212,'first_name':'Angela','last_name':'Crawford','pin_number':9999}
tutor1 = Tutor(t1)
tutor2 = Tutor(t2)
tutor3 = Tutor(t3)

s1 = {'first_name':"Angie",'last_name':"McKeown",'student_number':23232323}
s2 = {'first_name':"Claire",'last_name':"McCann",'student_number':67930223}
s3 = {'first_name':"Simon",'last_name':"Ferguson",'student_number':90276423}
s4 = {'first_name':"Ian",'last_name':"Thompson",'student_number':12093723}
s5 = {'first_name':"Kiera",'last_name':"Gosford",'student_number':58462223}
s6 = {'first_name':"Emma",'last_name':"Wilson",'student_number':25424323}
student1 = Student(s1)
student2 = Student(s2)
student3 = Student(s3)
student4 = Student(s4)
student5 = Student(s5)
student6 = Student(s6)

r1 = {'course_number':10001,'student_number':23232323,'attendance_data':"xoooxoxo",'present':5,'absent':3}
r2 = {'course_number':10001,'student_number':67930223,'attendance_data':"ooooxoxo",'present':6,'absent':2}
r3 = {'course_number':10002,'student_number':90276423,'attendance_data':"ooooxooo",'present':7,'absent':1}
r4 = {'course_number':10002,'student_number':12093723,'attendance_data':"oxooooxo",'present':6,'absent':2}
r5 = {'course_number':10002,'student_number':58462223,'attendance_data':"ooooxooo",'present':7,'absent':1}
r6 = {'course_number':10002,'student_number':23232323,'attendance_data':"oooooooo",'present':8,'absent':0}
record1 = Record(r1)
record2 = Record(r2)
record3 = Record(r3)
record4 = Record(r4)
record5 = Record(r5)
record6 = Record(r6)

# def import_data():  #  open the file that contains all the previously stored college information
#     fname = "college_records.txt"
#     try:
#         fh = open(fname)
#     except:
#         print('>>> Sorry, there was a problem opening the records file:',fname)
#         quit()
#     # now deal with the imported data correctly
#     for line in fh:
#         line = line.rstrip()  # take out the newlines at the ends
#         position = line.find('(') #Grab the position of the bracket
#         contents = str(line[position+1:-1])
#         if line.startswith('Course'):  # add to courses
#             pass
#         if line.startswith('Tutor'):
#             #add to tutors
#             pass
#         if line.startswith('Student'):
#             #add to students
#             pass
#         if line.startswith('Record'):
#             #add to????
#             pass

#import_data()  #  open the file that contains all the previously stored college information

############################### SECURITY CHECKS ################################
def is_num(input_number, range1, range2):  # checks the input is a decimal, and is between the target numbers
    if input_number.isdecimal() and ((range1-1) < int(input_number) < (range2+1)):
        return True
    else:
        print("\nSorry, you have to enter a number from " + str(range1) + " to " +
        str(range2) + ". Try again.")
        time.sleep(1)
        return False

def string_clean(input_string):  # cleans string of extraneous, possibly dangerous characters
    input_string = input_string.translate(input_string.maketrans('','',string.punctuation))
    input_string = input_string.translate(input_string.maketrans('','',string.digits))
    #input_string = input_string.translate(input_string.maketrans('','',string.numbers))
    return input_string

def access_granted(staff_num, pin_num):
    staff_num = int(staff_num)
    pin_num = int(pin_num)
    if college.check_pin_matches(staff_num, pin_num):
        return True
    return False

############################### INPUT FUNCTIONS ################################

def input_student(staff_num, pin_num):
    student_params = {}  # build a dict of all input to pass to Tutor
    #get student_number eg: 23232323
    temp_num = input("\nAssign a New Student Number (8 digits): ")
    if is_num(temp_num,10000000, 99999999):
        if not college.check_student_exists(temp_num):
            student_params['student_number'] = temp_num
        else:
            print("That student number already exists. Try Again")
            input_student(staff_num, pin_num)
    else:
        input_student(staff_num, pin_num)
    #get first_name eg: "Angie"
    temp_name = input("\nEnter their First Name: ")
    temp_name = string_clean(temp_name)
    student_params['first_name'] = temp_name

    #get last_name eg: "McKeown"
    temp_name = input("\nEnter their Last Name: ")
    temp_name = string_clean(temp_name)
    student_params['last_name'] = temp_name

    finaliseText = "Student Details To Add"
    print(finaliseText.center(72,"-"))
    print("\n Student Number: " + student_params['student_number'])
    print("   First Name: " + student_params['first_name'])
    print("    Last Name: " + student_params['last_name'])

    finalise = input("\nAre these details correct? (choose N to return to the Student Menu without saving) Y/N: ")
    if finalise.lower() == "y":
        final_student = Student(student_params)
        college.add_student(final_student)
        print("Student added successfully")
        time.sleep(1)
        menu_view_student_information(staff_num, pin_num)
    else:
        time.sleep(1)
        menu_view_student_information(staff_num, pin_num)


def input_tutor(staff_num, pin_num):
    tutor_params = {}  # build a dict of all input to pass to Tutor
    #get tutor_number eg: 278459
    temp_num = input("\nAssign a New Staff Number (6 digits): ")
    if is_num(temp_num,100000, 999999):
        if not college.check_staff_exists(temp_num):
            tutor_params['staff_number'] = temp_num
        else:
            print("That staff number already exists. Try Again")
            input_tutor(staff_num, pin_num)
    else:
        input_tutor(staff_num, pin_num)
    #get first_name eg: "Kris"
    temp_name = input("\nEnter their First Name: ")
    temp_name = string_clean(temp_name)
    tutor_params['first_name'] = temp_name

    #get last_name eg: "Jones"
    temp_name = input("\nEnter their Last Name: ")
    temp_name = string_clean(temp_name)
    tutor_params['last_name'] = temp_name

    #get pin_num eg: 1234
    temp_cost = input("\nAssign a Pin Number (4 digits): ")
    if is_num(temp_cost,1000, 9999):
        tutor_params['pin_number'] = temp_cost

    finaliseText = "Staff Details To Add - Please make a note of these!"
    print(finaliseText.center(72,"-"))
    print("\n Staff Number: " + tutor_params['staff_number'])
    print("   First Name: " + tutor_params['first_name'])
    print("    Last Name: " + tutor_params['last_name'])
    print(" Assigned Pin: " + tutor_params['pin_number'])

    finalise = input("\nAre these details correct? (choose N to return to the Tutor Menu without saving) Y/N: ")
    if finalise.lower() == "y":
        final_tutor = Tutor(tutor_params)
        college.add_tutor(final_tutor)
        print("Tutor added successfully")
        time.sleep(1)
        menu_view_tutor_information(staff_num, pin_num)
    else:
        time.sleep(1)
        menu_view_tutor_information(staff_num, pin_num)

def input_course(staff_num, pin_num):
    course_params = {}  # build a dict of all input to pass to Course
    #get course_number eg: 10002
    temp_num = input("\nEnter the New Course Number: ")
    if is_num(temp_num,10000, 99999):
        if not college.check_course_exists(temp_num):
            course_params['course_number'] = temp_num
        else:
            print("That course number already exists, Try Again")
            input_course(staff_num, pin_num)
    else:
        input_course(staff_num, pin_num)
    #get course_name eg: "Intermediate Python"
    temp_name = input("\nEnter the New Course Name: ")
    temp_name = string_clean(temp_name)
    course_params['course_name'] = temp_name

    #get course_description eg: "A short course in more advanced Python topics"
    temp_desc = input("\nEnter the Description: ")
    temp_desc = string_clean(temp_desc)
    course_params['course_description'] = temp_desc

    #get course_start eg: 2019-5-4
    temp_date = input("\nEnter the Date it Starts as DD-MM-YY: ")
    course_params['course_start'] = temp_date  # add date verification here

    temp_day = input("\nEnter the Day it runs on: ")
    #get course_day eg: "Thurs"
    temp_day = string_clean(temp_day)
    course_params['course_day'] = temp_day

    #get course_time eg: 1800
    temp_time = input("\nEnter the Time it starts at (24hr clock): ")
    if is_num(temp_time,0000, 2400):
        course_params['course_time'] = temp_time

    #get course_duration eg: 10
    temp_duration = input("\nEnter how many Weeks it runs for: ")
    if is_num(temp_duration,1, 208):
        course_params['course_duration'] = temp_duration

    #get course_cost eg: 10
    temp_cost = input("\nEnter how much it Costs: ")
    if is_num(temp_cost,0, 99999):
        course_params['course_cost'] = temp_cost

    #get course_certified eg: True
    temp_cert = input("\nIs there a Certificate? Y/N: ")
    if temp_cert.lower() == "y":
        course_params['course_certified'] = True
    else:
        course_params['course_certified'] = False

    #get course_isactive eg: True
    temp_isactive = input("\nIs it Open for Enrollment now? Y/N: ")
    if temp_isactive.lower() == "y":
        course_params['course_isactive'] = True
    else:
        course_params['course_isactive'] = False

    finaliseText = "Course Details To Add"
    print(finaliseText.center(72,"-"))
    print("\n Course Number: " + course_params['course_number'])
    print("   Course Name: " + course_params['course_name'])
    print("   Description: " + course_params['course_description'])
    print("    Start Date: " + course_params['course_start'])
    print("    Course Day: " + course_params['course_day'])
    print("   Course Time: " + course_params['course_time'])
    print("Weeks duration: " + course_params['course_duration'])
    print("   Course cost: " + course_params['course_cost'])
    print("   Certificate: " + str(course_params['course_certified']))
    print(" Enrolling now: " + str(course_params['course_isactive']))
    finalise = input("\nAre these details correct? (choose N to return to the Course Menu without saving) Y/N: ")
    if finalise.lower() == "y":
        final_course = Course(course_params)
        college.add_course(final_course)
        print("Course added successfully")
        time.sleep(1)
        menu_view_course_information(staff_num, pin_num)
    else:
        time.sleep(1)
        menu_view_course_information(staff_num, pin_num)

################################## MAIN MENUS ##################################

def menu_view_security(staff_num, pin_num):
    if access_granted(staff_num, pin_num):
        print("\nSECURITY MENU >>> Please choose from the following options:")
        print("----------------------------------------------------------\n")
        print(" 1) View a list of Security Logs (admin only)")
        print(" 2) Assign Admin Priviledges to an existing tutor (admin only)")
        print(" 3) Remove Admin Priviledges to an existing tutor (admin only)")
        print(" 4) Return to the Main Menu")
        menu_choice = input("\n : ")
        if is_num(menu_choice, 1, 4):  # check it's a number and within expected range
            menu_choice = int(menu_choice)
        else:
            menu_view_security(staff_num, pin_num)

        if menu_choice == 4:  # Return to the Main Menu
            main_menu(staff_num, pin_num)

        elif menu_choice == 1:  # print security logs
        # do an admin check
            print("When implemented, this function will print security logs")
            college.print_security_logs()
            time.sleep(1)
            menu_view_security(staff_num, pin_num)

        elif menu_choice == 2:  # give admin privs
        # do an admin check, input tutor num
            print("When implemented, this function will give admin privs")
            #college.add_admin(tutor_num)
            time.sleep(1)
            menu_view_security(staff_num, pin_num)

        elif menu_choice == 3:  # remove admin privs
        # do an admin check, input tutor num
            print("When implemented, this function will remove admin privs")
            #college.remove_admin(tutor_num)
            time.sleep(1)
            menu_view_security(staff_num, pin_num)



def menu_view_tutor_information(staff_num, pin_num):
    if access_granted(staff_num, pin_num):
        print("\nTUTORS MENU >>> Please choose from the following options:")
        print("----------------------------------------------------------\n")
        print(" 1) View a list of all Tutors")
        print(" 2) View Information about a specific Tutor (admin only)")
        print(" 3) Add a Tutor to the Staff register (admin only)")
        print(" 4) Add a Tutor to a specific Course (admin only)")
        print(" 5) Remove a Tutor from a specific Course (admin only)")
        print(" 6) Remove a Tutor from the Staff register (admin only)")
        print(" 7) Return to the Main Menu")
        menu_choice = input("\n : ")
        if is_num(menu_choice, 1, 7):  # check it's a number and within expected range
            menu_choice = int(menu_choice)
        else:
            menu_view_tutor_information(staff_num, pin_num)

        if menu_choice == 7:  # Return to the Main Menu
            main_menu(staff_num, pin_num)

        elif menu_choice == 1:  # print all the courses
            college.print_tutors()
            time.sleep(1)
            menu_view_tutor_information(staff_num, pin_num)

        elif menu_choice == 2:  # Go to the View Specific Tutor menu (admin only)
            if college.admin_check(staff_num):
                tutor_choice = input("Enter the tutor number: ")
                if is_num(tutor_choice, 100000, 999999):
                    college.get_tutor_full_details(tutor_choice)  # Display all info about the tutor here
                    time.sleep(1)
                    menu_view_tutor_information(staff_num, pin_num)
                else:
                    menu_view_tutor_information(staff_num, pin_num)
            else:
                print("\n** Sorry, you must be an ADMIN to View Tutor Information **")
                time.sleep(1)
                menu_view_tutor_information(staff_num, pin_num)

        elif menu_choice == 3:  # Add a tutor to the staff register (admin only)
            if college.admin_check(staff_num):
                input_tutor(staff_num, pin_num)
            else:
                print("\n** Sorry, you must be an ADMIN to Add a Tutor to Staff **")
                time.sleep(1)
                menu_view_tutor_information(staff_num, pin_num)

        elif menu_choice == 4:  # Add a tutor to a specific course (admin or only)
            if college.admin_check(staff_num):  # check if they are an admin
                course_num = input("Enter the Course number: ")  # now get the course number and the tutor number
                if is_num(course_num, 10000, 99999):
                    tutor_num = input("Enter the Tutor number: ")
                    if is_num(tutor_num, 100000, 999999):
                        college.add_tutor_to_course(tutor_num, course_num)
                        time.sleep(1)
                        menu_view_tutor_information(staff_num, pin_num)
            else:
                print("\n** Sorry, you must be an ADMIN to Add a Tutor to a Course **")
                time.sleep(1)
                menu_view_tutor_information(staff_num, pin_num)

        elif menu_choice == 5:  # Remove a tutor from a specific course (admin only)
            if college.admin_check(staff_num):
                course_num = input("Enter the Course number: ")
                if is_num(course_num, 10000, 99999):
                    tutor_num = input("Enter the Tutor number: ")
                    if is_num(tutor_num, 100000, 999999):
                        college.remove_tutor_from_course(tutor_num, course_num)
                        time.sleep(1)
                        menu_view_tutor_information(staff_num, pin_num)
                    else:
                        time.sleep(1)
                        menu_view_tutor_information(staff_num, pin_num)
                else:
                    time.sleep(1)
                    menu_view_tutor_information(staff_num, pin_num)
            else:
                print("\n** Sorry, you must be an ADMIN to Remove a Tutor from a Course**")
                time.sleep(1)
                menu_view_tutor_information(staff_num, pin_num)

        elif menu_choice == 6:  # Remove a tutor from ALL courses (admin only)
            if college.admin_check(staff_num):
                temp_num = input("\nEnter the Staff Number to Remove: ")
                if is_num(temp_num,100000, 999999):
                    if temp_num != staff_num:
                        college.remove_tutor(temp_num)
                        time.sleep(1)
                        menu_view_tutor_information(staff_num, pin_num)
                    else:
                        print("You can't remove yourself! Try Again")
                        time.sleep(1)
                        menu_view_tutor_information(staff_num, pin_num)
                menu_view_tutor_information(staff_num, pin_num)
            else:
                print("\n** Sorry, you must be an ADMIN to Remove a Tutor from the Staff register **")
                time.sleep(1)
                menu_view_tutor_information(staff_num, pin_num)

        else:
            print("An error has occurred, please contact the office.")
            main_menu(staff_num, pin_num)


def menu_view_student_information(staff_num, pin_num):
    if access_granted(staff_num, pin_num):
        print("\nSTUDENTS MENU >>> Please choose from the following options:")
        print("----------------------------------------------------------\n")
        print(" 1) View info/attendance for a specific student")
        print(" 2) Add a student to your course")
        print(" 3) Remove a student from your course")
        print(" 4) View a list of all students (admin only)")
        print(" 5) Enroll a student in the college (admin only)")
        print(" 6) Remove a student from all courses (admin only)")
        print(" 7) Return to the Main Menu")

        menu_choice = input("\n : ")
        if is_num(menu_choice, 1, 7):  # check it's a number and within expected range
            menu_choice = int(menu_choice)
        else:
            menu_view_student_information(staff_num, pin_num)
        if menu_choice == 7:  # Return to Main Menu
            main_menu(staff_num, pin_num)

        elif menu_choice == 1:  # View specific student
            student_choice = input("Enter the student number: ")
            if is_num(student_choice, 10000000, 99999999):
                college.get_student_full_details(student_choice)
                time.sleep(1)
                menu_view_student_information(staff_num, pin_num)
            else:
                menu_view_student_information(staff_num, pin_num)

        elif menu_choice == 2:  # Add a student
            #get the course number and the student number
            course_num = input("Enter the Course number: ") # now check if they are tutor on this course, or an admin
            if is_num(course_num, 10000, 99999):
                if college.check_tutor_on_course(staff_num, course_num) or college.admin_check(staff_num):
                    student_num = input("Enter the Student number: ")
                    if is_num(student_num, 10000000, 99999999):
                        college.add_student_to_course(student_num, course_num)
                        time.sleep(1)
                        menu_view_student_information(staff_num, pin_num)
                else:
                    print("** Sorry, you have to be an Admin or the Course Tutor to add a Student **")
            time.sleep(1)
            menu_view_student_information(staff_num, pin_num)

        elif menu_choice == 3:  # Remove a student
            #get the course number and the student number
            course_num = input("Enter the Course number: ") # now check if they are tutor on this course, or an admin
            if is_num(course_num, 10000, 99999):
                if college.check_tutor_on_course(staff_num, course_num) or college.admin_check(staff_num):
                    student_num = input("Enter the Student number: ")
                    if is_num(student_num, 10000000, 99999999):
                        college.remove_student_from_course(student_num, course_num)
                        time.sleep(1)
                        menu_view_student_information(staff_num, pin_num)
                else:
                    print("** Sorry, you have to be an Admin or the Course Tutor to remove a Student **")
            time.sleep(1)
            menu_view_student_information(staff_num, pin_num)

        elif menu_choice == 4:  # print all the students (admin only)
            if college.admin_check(staff_num):
                college.print_students()
                time.sleep(1)
                menu_view_student_information(staff_num, pin_num)
            else:
                print("\n** Sorry, you must be an ADMIN to view the full student list **")
                time.sleep(1)
                menu_view_student_information(staff_num, pin_num)

        elif menu_choice == 5:  # Enroll a student in the college (admin only)
            if college.admin_check(staff_num):
                input_student(staff_num, pin_num)
            else:
                print("\n** Sorry, you must be an ADMIN to Enroll a Student **")
                time.sleep(1)
                menu_view_student_information(staff_num, pin_num)

        elif menu_choice == 6:  # Remove a student from all courses (admin only)
            if college.admin_check(staff_num):
                pass
                #STUFF GOES HERE
            else:
                print("\n** Sorry, you must be an ADMIN to Remove a Student **")
                time.sleep(1)
                menu_view_student_information(staff_num, pin_num)

        else:
            print("An error has occurred, please contact the office.")
            main_menu(staff_num, pin_num)


def menu_view_course_information(staff_num, pin_num):
    if access_granted(staff_num, pin_num):
        print("\nCOURSES MENU >>> Please choose from the following options:")
        print("----------------------------------------------------------\n")
        print(" 1) View info/attendance for a specific course")
        print(" 2) View a list of all courses")
        print(" 3) Add a course (admin only)")
        print(" 4) Remove a course (admin only)")
        print(" 5) Return to the Main Menu")
        menu_choice = input("\n : ")
        if is_num(menu_choice, 1, 5):  # check it's a number and within expected range
            menu_choice = int(menu_choice)
        else:
            menu_view_course_information(staff_num, pin_num)
        if menu_choice == 5:
            main_menu(staff_num, pin_num)
        elif menu_choice == 1:  # View info/attendance for a specific course
            course_choice = input("Enter the course number: ")
            if is_num(course_choice, 10000, 99999):
                    college.get_full_course_details(course_choice)
                    college.show_tutors_on_course(course_choice)  # Display all info about the course here
                    if college.admin_check(staff_num) or college.check_tutor_on_course(staff_num, course_choice):
                        college.show_students_on_course(course_choice)
                    else:
                        college.show_class_size(course_choice)  # Display how many students are currently enrolled
                    time.sleep(1)
                    menu_view_course_information(staff_num, pin_num)

            else:
                menu_view_course_information(staff_num, pin_num)
        elif menu_choice == 2:  # print all the courses
            college.print_courses()
            time.sleep(1)
            menu_view_course_information(staff_num, pin_num)
        elif menu_choice == 3:  # Add a course (admin only)
            if college.admin_check(staff_num):
                input_course(staff_num, pin_num)
            else:
                print("\n** Sorry, you must be an ADMIN to Add a Course **")
                time.sleep(1)
                menu_view_course_information(staff_num, pin_num)
        elif menu_choice == 4:  # Remove a course (admin only) for record purposes this only makes it unenrollable
            if college.admin_check(staff_num):
                temp_num = input("\nEnter the Course Number to Remove: ")
                if is_num(temp_num,10000, 99999):
                    if college.check_course_exists(temp_num):
                        college.remove_course(temp_num)
                        time.sleep(1)
                        menu_view_course_information(staff_num, pin_num)
                    else:
                        print("That course number doesn't exist. Try Again")
            else:
                print("\n** Sorry, you must be an ADMIN to Remove a Course **")
                time.sleep(1)
                menu_view_course_information(staff_num, pin_num)
        else:
            print("An error has occurred, please contact the office.")
            main_menu(staff_num, pin_num)


def main_menu(staff_num, pin_num):
    print("\nPlease choose from the following options:\n")
    print(" 1) View Course information\n 2) View Student information")
    print(" 3) View Tutor information \n 4) View Security menu")
    print(" 5) EXIT")
    menu_choice = input("\n : ")
    if is_num(menu_choice, 1, 5):  # check it's a number and within expected range
        menu_choice = int(menu_choice)
    else:
        main_menu(staff_num, pin_num)
    if menu_choice == 5:
        exit()
    elif menu_choice == 1:  # Go to the course menu
        menu_view_course_information(staff_num, pin_num)
    elif menu_choice == 2:  # Go to the student menu
        menu_view_student_information(staff_num, pin_num)
    elif menu_choice == 3:  # Go to the tutor menu
        menu_view_tutor_information(staff_num, pin_num)
    elif menu_choice == 4:  # Go to the security menu
        menu_view_security(staff_num, pin_num)
    else:
        print("An error has occurred, please contact the office.")
        main_menu(staff_num, pin_num)

def enter_your_pin(count, staff_num):  # Enter your pin, check if it's correct, if so then call the main_menu function
    if count == 5:  # Give the user 5 chances to enter their pin number
        print("Sorry, too many tries! Your account has been locked, please contact the office.")
        exit()
    inp_pin = input("Please enter your pin number: ")
    if is_num(inp_pin, 1000, 9999):
        if access_granted(staff_num, inp_pin):
            main_menu(staff_num, inp_pin)
        else:
            print("Wrong pin number! (Attempt " + str((count + 1)) + " of 5)")
            count = count + 1
            enter_your_pin(count, staff_num)
    else:
        enter_your_pin(count, staff_num)


def enter_your_staff_number(count):  # Enter staff_number, check if it exists, if it does then call enter_your_pin function
    if count == 5:  # Give the user 5 chances to enter an existing username
        print("Sorry, too many tries, please contact the office.")
        exit()
    else:
        inp_staff_num = input("Please enter your staff number: ")  # check if the staff number exists
        if is_num(inp_staff_num, 100000, 999999):
            if college.check_staff_exists(inp_staff_num):
                enter_your_pin(0, inp_staff_num)
            else:
                print("That staff number does not exist! (Attempt " + str((count+1)) + " of 5)")
                count = count + 1
                enter_your_staff_number(count)
        else:
            enter_your_staff_number(count)

############################# DATA INITIALISATION ##############################
college.add_course(course1)
college.add_course(course2)
college.add_course(course3)

college.add_tutor(tutor1)
college.add_tutor(tutor2)
college.add_tutor(tutor3)
college.add_admin(987654)

college.add_student(student1)
college.add_student(student2)
college.add_student(student3)
college.add_student(student4)
college.add_student(student5)
college.add_student(student6)

college.add_record(record1)
college.add_record(record2)
college.add_record(record3)
college.add_record(record4)
college.add_record(record5)
college.add_record(record6)

college.add_student_to_course(23232323, 10001)
college.add_student_to_course(12093723, 10001)
college.add_student_to_course(90276423, 10001)
college.add_student_to_course(58462223, 10001)
college.add_student_to_course(25424323, 10001)
college.add_student_to_course(67930223, 10001)

college.add_student_to_course(58462223, 10002)
college.add_student_to_course(23232323, 10002)
college.add_student_to_course(67930223, 10002)
college.add_student_to_course(12093723, 10002)
college.add_student_to_course(90276423, 10002)


college.add_tutor_to_course(987654, 10001)
college.add_tutor_to_course(987654, 10002)
college.add_tutor_to_course(278459, 10001)
college.add_tutor_to_course(121212,10002)

"""The menu should allow you to:
  -record course attendance (if authorised, or God)
  -viewing student attendance (if authorised for that course, or God)
  -viewing course attendance (if authorised for that course, or God)
---add a student (if God)--------------------------------------
  -remove a student (from a course, from all courses)
---add a teacher (to a selection of courses)-------------------
---add a course (if God)---------------------------------------
  -remove a course (from active courses)"""

################################ PROGRAM START #################################

print("\n"*10,"_~_~_~_~_~_~_~_~ " + college.name + " ~_~_~_~_~_~_~_~_\n\n\n")
print(image + "\n")
enter_your_staff_number(0)  # begin the login process, pass 0 to count
