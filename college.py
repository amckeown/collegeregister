class College:
    """College (summarize its behavior and list the public methods)

     Performs authentication functions, keeps
     lists of available courses, tutors and students and their status, and all
     methods which manipulate these

     """

    def __init__(self, name):
        self.name = name
        self.course_list = []
        self.tutor_list = []
        self.student_list = []
        self.record_list = []
        self.course_tutors_dict = dict()
        self.course_students_dict = dict()
        self.admin_privs_list = []

############################### SECURITY CHECKS ################################

    def check_pin_matches(self, inp_staff_num, inp_pin):  # only return the tutor whose staff_number AND pin match those given
        inp_staff_num = int(inp_staff_num)
        inp_pin = int(inp_pin)
        for tutor in self.tutor_list:
            tutor_num = tutor.get_staff_number()
            tutor_pin = tutor.get_pin_number()
            if (inp_staff_num == tutor_num) and (inp_pin == tutor_pin):
                return tutor
        return False


    def admin_check(self, inp_tutor): #checks if someone is on the privs list
        inp_tutor = int(inp_tutor)
        for tutor in self.admin_privs_list:
            if inp_tutor == tutor:
                return True
        return False


    def check_student_exists(self, inp_student_num):  # checks the student list for a student number which matches the one input
        inp_student_num = int(inp_student_num)
        for student in self.student_list:
            student_num = student.get_student_number()
            if inp_student_num == student_num:
                return student
        return False


    def check_course_exists(self, inp_course_num):  # checks the course list for a course number which matches the one input
        inp_course_num = int(inp_course_num)
        for course in self.course_list:
            course_num = course.get_course_number()
            if inp_course_num == course_num:
                return course
        return False


    def check_staff_exists(self, inp_staff_num):  # checks the tutor list for a staff number which matches the one input
        inp_staff_num = int(inp_staff_num)
        for tutor in self.tutor_list:
            tutor_num = tutor.get_staff_number()
            if inp_staff_num == tutor_num:
                return tutor
        return False


    def check_tutor_on_course(self, tutor_num, course_num):  # check if specific tutor_num is on specific course_num
        tutor_num = int(tutor_num)
        course_num = int(course_num)
        staff_list = self.course_tutors_dict[course_num].rstrip(',')
        for tutor in staff_list:
            if tutor_num == tutor:
                return True
        return False


    def print_security_logs(self):  # print a list of who added and removed things
        print("\nSecurity Logs\n----------------")
        for course in self.course_list:
            course.show_course_security_logs()
        # also add students and tutors lists here if this one works in Test


    def add_admin(self, tutor): #add a tutor to the admin list
        #check you are an admin first
        self.admin_privs_list.append(tutor)
        print("Tutor " + str(tutor) + " successfully granted Admin priviledges")


    def remove_admin(self, tutor):  # remove a tutor from the admin list
        self.admin_privs_list.remove(tutor)
        print("Tutor " + str(tutor) + " Admin priviledges successfully removed")

############################## ALL COURSE METHODS ##############################

    def find_course(self, inp_course):  # find out if a course number is in the list
        inp_course = int(inp_course)
        for course in self.course_list:
            course_num = course.get_course_number()
            if course_num == inp_course:
                return course
        return False

    def add_course(self, course): #add a course to the cirriculum
        self.course_list.append(course)

    def remove_course(self, course_num):  # deactivate a course
        course_num = int(course_num)
        # set the 'isactive' value to 'not currently available'
        course = self.find_course(course_num) # course obj
        if course.get_course_status():
            course.change_course_status()
        print(str(course.get_course_number()) + " " + course.get_course_name() + " course removed.")
        print("You can no longer Add Students, Tutors or Attendance to this Course")

    def show_students_on_course(self, course_num): #why does this only get 2 objs instead of 5 or 6????
        course_num = int(course_num)
        course = self.find_course(course_num)
        if course:
            count = 0
            attendance = "Attendance (x:absent, o:present):\n"
            records = self.find_record_by_course(course_num)
            for record in records:
                print(records)
                count += 1
                student_num = record.get_student_number()
                student = self.find_student(student_num)
                student_name = student.get_full_name()
                attendance += ("[" + str(student_num) + "] " + (student_name) + "-" + record.get_attendance_data() + " - " + str(record.get_presence()) + " classes attended.\n")
            print("~"*92)
            print(str(count) + " students currently enrolled.")
            print(attendance)
            print("-"*92)
            if course.get_course_status():
                rollcall = input("Would you like to take attendance for this course now? Y/N: ")
                if rollcall.lower() == "y":
                    self.record_attendance_by_course(records)
                else:
                    return
        else:
            print("That course doesn't exist, sorry.")
            return


    def show_class_size(self, course_num):  # show students on course_num in course_students_dict
        course_num = int(course_num)
        try:
            student_list = self.course_students_dict[course_num].rstrip(',')
            list = student_list.split(',')
            count = 0
            for number in list:
                count += 1
                student = self.check_student_exists(number)
            print("Class size: " + str(count))
        except:
            print("No students currently enrolled")


    def get_course_details(self, course): # print the short course details
        active = course.get_course_status()
        if active == True:
            active = str("(Open for Enrollment)")
        else: active = str("(Not Currently Available)")
        print("\n" + str(course.get_course_number()) + " - " + course.get_course_name() + " - " + active, end = " ")

    def get_full_course_details(self, course_num):
        course = self.find_course(course_num)  # Check it's a real course
        if course: # print the full course details
        #print("Name, description, start date, day, time, duration, cost, certified, tutors.")
            active = course.get_course_status()
            if active == True:
                active = str("(Open for Enrollment)")
            else: active = str("(Not Currently Available)")
            cost = course.get_course_cost()
            if cost == 0:
                cost = "Free"
            print("~"*92)
            print(str(course.get_course_number()) + " - " + course.get_course_name() + " - " + active)
            print("-"*92)
            print(str(course.get_course_description()))
            print("-"*92)
            print("Day & Time: " + str(course.get_course_day()) + " at " + str(course.get_course_time()) + "hrs, for " + str(course.get_course_duration()) + " weeks                                 Start Date: " + str(course.get_course_start()))
            print("Cost: " + str(cost) + "                                                                Certified: " + str(course.get_course_certified()))
            print("_"*92)
        else:
            print("That course doesn't exist, sorry.")
            return


    def print_courses(self): #print a list of all courses
        print("\nCurrent Courses\n----------------")
        for course in self.course_list:
            self.get_course_details(course)
            try:
                student_list = self.course_students_dict[course.get_course_number()].rstrip(',')
                list = student_list.split(',')
                count = 0
                for number in list:
                    count += 1
                print("[" + str(count) + " enrolled students]")
            except:
                print("No students currently enrolled")


############################## ALL STUDENT METHODS #############################
    def find_student(self, inp_student):  # find out if a student number is in the list
        inp_student = int(inp_student)
        for student in self.student_list:
            student_num = student.get_student_number()
            if student_num == inp_student:
                return student
        return False

    def add_student(self, student): #add a student to the college
        self.student_list.append(student)

    def remove_student(self, student):  # deactivate a student
        print("student removed")

    def add_student_to_course(self, student_num, course_num): #add a student to a course
        course_num = int(course_num)
        student_num = int(student_num)
        if self.check_course_exists(course_num) and self.check_student_exists(student_num):
            try:
                self.course_students_dict[course_num] = self.course_students_dict.get(course_num, "") + (str(student_num) + ",")
                print("Student " + str(student_num) + " added to course " + str(course_num))
            except:
                print("An error has occurred. Student " + str(student_num) + " not added to course " + str(course_num))
        else:
            print("Error: does not exist")  # This would be improved if it said 'no such student' or 'no such course'


    def remove_student_from_course(self, student_num, course_num): #remove a student from a course
        course_num = int(course_num)
        student_num = int(student_num)
        if (self.check_course_exists(course_num)) and (self.check_student_exists(student_num)):
            try:
                self.course_students_dict[course_num] = self.course_students_dict[course_num].replace(str(student_num)+',','')  # note that it has to take the trailing comma with it to avoid breaking things!
                print("Student " + str(student_num) + " removed from course " + str(course_num))
            except:
                print("An error has occurred. Student " + str(student_num) + " not removed from course " + str(course_num))
        else:
            print("Error: does not exist")  # This would be improved if it said 'no such student' or 'no such course'


    def get_student_full_details(self, student_num):
        count = 0
        courses = ""
        student = self.find_student(student_num)  # student obj
        if student:
            records = self.find_record_by_student(student_num)  #records obj
            attendance = "Attendance (x:absent, o:present):\n"
            for record in records:
                count += 1
                course_num = record.get_course_number()
                course = self.find_course(course_num)  # course obj
                course_name = course.get_course_name()
                courses = (courses + str(course_num) + " " + course_name + ", ")
                attendance += (str(course_num)) + "-" + (record.get_attendance_data() + " - " + str(record.get_presence()) + " classes attended.\n")
            print("~"*92)
            print("[" + str(student_num) + "] " + student.get_full_name())
            print(str(count) + " current courses: " + courses.rstrip(', '))
            print("-"*92)
            print(attendance)
            print("-"*92)
            rollcall = input("Would you like to take attendance for this course now? Y/N: ")
            if rollcall.lower() == "y":
                self.record_attendance_by_student(records)
            else:
                return
        else:
            print("That student doesn't exist, sorry.")
            return


    def print_students(self): #print a list of all students
        print("\nCurrent Students\n----------------")
        for student in self.student_list:
            print(student.get_full_name() + " (" + str(student.get_student_number()) + ")")

############################### ALL TUTOR METHODS ##############################

    def find_tutor(self, inp_tutor):  # find out if a tutor number is in the list
        inp_tutor = int(inp_tutor)
        for tutor in self.tutor_list:
            staff_num = tutor.get_staff_number()
            if staff_num == inp_tutor:
                return tutor
        return False


    def add_tutor(self, tutor): #add a tutor to the staff list
        self.tutor_list.append(tutor)
        #set the tutor.set_tutor_added(date)
        #set the tutor.set_tutor_addedby(staff_num)


    def remove_tutor(self, tutor_num):  # remove a tutor completely
        tutor_num = int(tutor_num)
        tutor = self.check_staff_exists(tutor_num)
        if tutor:
            self.tutor_list.remove(tutor)
            for key, value in self.course_tutors_dict.items():
                value = value.rstrip(',')
                list = value.split(',')
                for number in list:
                    if number == tutor_num:
                        (self.course_tutors_dict[key]).replace(tutor_num,'') # this is not working, needs deleted, not replaced with white space
        else:
            print("That Staff number doesn't exist. Try Again")


    def get_all_courses_tutor_is_on(self, tutor_num):
        count = 0
        courses = ""
        for key, value in self.course_tutors_dict.items():
            value = value.rstrip(',')
            list = value.split(',')
            for number in list:
                if number == tutor_num:
                    count += 1
                    course = self.find_course(key)
                    course_name = course.get_course_name()
                    key = str(key)
                    courses = (courses + key + " " + course_name + ", ")
        courses = courses.rstrip(', ')
        return count, courses


    def get_tutor_full_details(self, tutor_num):
        tutor = self.find_tutor(tutor_num)  # Check it's a real tutor
        if tutor:
            count, courses = self.get_all_courses_tutor_is_on(tutor_num)
            print("~"*92)
            print("[" + str(tutor_num) + "] " +  tutor.get_full_name(), end = ' ')
            if self.admin_check(tutor_num):print("{has Administrator priviledges}")
            print("\n" + str(count) + " current courses: " + courses.rstrip(', '))
            print("-"*92)
        else:
            print("That student doesn't exist, sorry.")
            return

    def add_tutor_to_course(self, tutor_num, course_num): #add a tutor to a course
        course_num = int(course_num)
        tutor_num = int(tutor_num)
        if self.check_course_exists(course_num) and self.check_staff_exists(tutor_num):
            try:
                self.course_tutors_dict[course_num] = self.course_tutors_dict.get(course_num, "") + (str(tutor_num) + ",")
                print("Tutor " + str(tutor_num) + " added to course " + str(course_num))
            except:
                print("An error has occurred. Tutor " + str(tutor_num) + " not added to course " + str(course_num))
        else:
            print("Error: does not exist.")  # This would be improved if it said 'no such tutor' or 'no such course'


    def remove_tutor_from_course(self, tutor_num, course_num): #remove a tutor from a course
        course_num = int(course_num)
        tutor_num = int(tutor_num)
        if (self.check_course_exists(course_num)) and (self.check_staff_exists(tutor_num)):
            try:
                self.course_tutors_dict[course_num] = self.course_tutors_dict[course_num].replace(str(tutor_num)+',','')  # note that it has to take the trailing comma with it to avoid breaking things!
                print("Tutor " + str(tutor_num) + " removed from course " + str(course_num))
            except:
                print("An error has occurred. Tutor " + str(tutor_num) + " not removed from course " + str(course_num))
        else:
            print("Error: does not exist")
            return  # This would be improved if it said 'no such tutor' or 'no such course'


    def show_tutors_on_course(self, course_num):  # show tutors on course_num in course_tutors_dict
        course_num = int(course_num)
        try:
            staff_list = self.course_tutors_dict[course_num].rstrip(',')
            list = staff_list.split(',')
            count = 0
            for number in list:
                count += 1
                tutor = self.check_staff_exists(number)
                print(tutor.get_full_name(), end = ", ")
            print("[" + str(count) + " teaching staff]")
        except:
            print("No tutor currently attached to this course")

    def print_tutors(self): #print a list of all tutors
        print("\nCurrent Staff\n----------------")
        for tutor in self.tutor_list:
            print(tutor.get_full_name() + " (" + str(tutor.get_staff_number()) + ")")


######################### ALL ATTENDANCE RECORD METHODS ########################

    def add_record(self, record): #add a record to the attendance record list
        self.record_list.append(record)

    def get_all_student_attendance(self, student_num):
        return

    def find_record_by_student(self, student_num):
        student_num = int(student_num)
        records = []
        for record in self.record_list:
            record_num = record.get_student_number()
            if record_num == student_num:
                records.append(record)
        return records

    def find_record_by_course(self, course_num):
        course_num = int(course_num)
        records = []
        for record in self.record_list:
            record_num = record.get_course_number()
            if record_num == course_num:
                records.append(record)
        return records

    def record_attendance_by_course(self, records):
        print("\nMark students Present with o or Absent with x")
        print("---------------------------------------------")
        for record in records:
            student_num = record.get_student_number()
            student = self.find_student(student_num)
            student_name = student.get_full_name()
            roll = input(student_name + ": ")
            if roll == "o":
                record.add_attendance_present()
            elif roll == "x":
                record.add_attendance_absent()
            else:
                print("Input not Recognised - Not Added")
                continue
        return

    def record_attendance_by_student(self, records):
        print("\nMark student Present with o or Absent with x, any other key to skip.")
        print("--------------------------------------------------------------------")
        for record in records:
            course_num = record.get_course_number()
            course = self.find_course(course_num)
            course_name = course.get_course_name()
            roll = input(course_name + ": ")
            if roll == "o":
                record.add_attendance_present()
            elif roll == "x":
                record.add_attendance_absent()
            else:
                print("Not Added")
                continue
        pass
