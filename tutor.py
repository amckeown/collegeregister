class Tutor:
    def __init__ (self, tutor_params):
        self.first_name = tutor_params.get('first_name')
        self.last_name = tutor_params.get('last_name')
        self.staff_number = int(tutor_params.get('staff_number'))
        self.pin_number = int(tutor_params.get('pin_number'))
        self.tutor_added = tutor_params.get('tutor_added')
        self.tutor_addedby = tutor_params.get('tutor_addedby')
        self.tutor_removed = tutor_params.get('tutor_removed')
        self.tutor_removedby = tutor_params.get('tutor_removedby')

    def get_full_name(self):
        return self.first_name + " " + self.last_name

    def get_staff_number(self):
        return self.staff_number

    def get_pin_number(self):
        return self.pin_number

    def set_tutor_added(self, date):
        self.tutor_added = date

    def set_tutor_addedby(self, staff_num):
        self.tutor_addedby = staff_num

    def set_tutor_removed(self, date):
        self.tutor_added = date

    def set_tutor_removedby(self, staff_num):
        self.tutor_addedby = staff_num
