class Record:
    def __init__(self, record_params):
        self.course_number = record_params.get('course_number')
        self.student_number = record_params.get('student_number')
        self.attendance_data = record_params.get('attendance_data')
        self.present = record_params.get('present')
        self.absent = record_params.get('absent')

    # Example Record(10001, 23232323, "----x-x-", 6, 2)
    # course_number, student_number, attendance_data, present, absent

    def get_course_number(self):
        return self.course_number

    def get_student_number(self):
        return self.student_number

    def get_attendance_data(self):
        return self.attendance_data

    def get_presence(self):
        return self.present

    def get_absence(self):
        return self.absent

    def add_attendance_present(self):
        self.present = self.present + 1
        self.attendance_data += 'o'
        return True

    def add_attendance_absent(self):
        self.absent = self.absent + 1
        self.attendance_data += 'x'
        return True
