
main.py
* loads the flat file containing the current courses, students, teachers,
  and attendance data (inc error handling)
$* loads the login screen (inc error handling)
$* loads the menu which allows:
$  -record course attendance (if authorised, or God)
$  -viewing student attendance (if authorised for that course, or God)
$  -viewing course attendance (if authorised for that course, or God)
$  -add a student (if God, or course tutor for that course)
  -remove a student (from your course, from all courses if god)
$ -add a teacher (to a selection of courses if god)
$ -add a course (if God)
$ -remove a course (from active courses)


courses.py is the courses class
  Each course should hold a list of who is authorised to moderate it (college.course_tutors_dict)
  Each course should hold a list of who is enrolled on it (college.course_students_dict)
  There is an Active Courses list
  Course(Name, Description, Start, Day, Time, Duration, Cost, Certified)

  Functions:
  - Add course (and to active courses list)
  - Remove course (from active courses list, but not completely or we'd lose data)
  - Print all students on this course
  - Print all teachers moderating this course
  - Record attendance for this course for [date] (default today, using students)


tutors.py is the teachers class
  Tutor(First Name, Last Name, Staff Number, Pin Number)

  Functions
  - Add teacher (to multiple courses)


students.py is the students class
  Student(First Name, Last Name, Student Number)

  Functions
  - Add student
  - Remove student (from one course, or multiple)


where is attendance stored? in courses?? in students?? both?? Neither - in college dict

how to handle attendance? using dates seems complicated.
Maybe week 1, week 2, week 3, etc?
if you attend you get an x, if you don't you get a -
this is easy to record and strip in a text file then

Attendance records:
Record(course number, student number, attendance, present, absent, Pass/Fail if certificate course)
Record(10001, 23232323, "----x-x-", 6, 2, "")


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BUG TRACKER

ADD smoothing
-------------
Try-Catches to everything not working
Remove all the bloody nested IFs

FIX
---
If you try to add a student to a course that doesn't exist, it just throws a Key Error



ONLY PICKING UP TWO attendance records at a time?????


ADD functionality
-----------------
*Remove tutors entirely
*Remove students entirely

*When Adding a teacher, ALSO select which courses to add them to at the same time

Change to make it impossible to Add students or Tutors
  to courses which are marked unavailable

ADD extras
----------
WHEN you do an ADMIN thing it should log WHO did it and WHEN
ADD 'Print security logs' to a menu somewhere
UPGRADE attendance to include a start date in case someone takes the same course twice
  (could be taken care of if unique course auto-numbering is enforced)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Menus
100% - 1) View Course information
100% - 2) View Student information
100% - 3) View Tutor information
100% - 7) EXIT

-----Consider adding another menu option to show security logs, but
      need to add and test the functionality for this first

>>>>> Course information
90% -- 1) View info/attendance for a specific course (add attendance) -------BROKEN
100% - 2) View a list of all courses
100% - 3) Add a course (admin only)
100% - 4) Remove a course (admin only)
100% - 5) Return to the Main Menu

-----consider adding another menu option to list only courses which are currently running


>>>>> Student information
100% - 1) View info/attendance for a specific student (add attendance)
100% - 2) Add a student to your course
100% - 3) Remove a student from your course
100% - 4) View a list of all students (admin only)
100% - 5) Enrol a student in the college (admin only)
6) Remove a student from all courses (admin only)-----------------------------
100% - 7) Return to the Main Menu

>>>>> Tutor Information
100% - 1) View a list of all Tutors
100% - 2) View Information about a specific Tutor (admin only)
100% - 3) Add a Tutor to the Staff register (admin only)
100% - 4) Add a Tutor to a specific Course (admin only)
100% - 5) Remove a Tutor from a specific Course (admin only)
50% -- 6) Remove a Tutor from the Staff register (admin only) ---------------BROKEN
100% - 7) Return to the Main Menu



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Original Specs
--------------
****Show a menu for the teacher to be able to interact with
****Hard code in a course for the students to be added to
****Add a students
****Take a roll for each students
--------------------------------------------------------
****Show a menu for the teacher with error handling
****Add a student to a specific course
Remove a student from a specific course, or all courses
****Add a course
****Remove a courses
****Take a roll for a specific course
--------------------------------------------------------
****Login as teacher via 4digit pin with error handling and get menu
****Show a menu for the teacher with error handling
****Take a roll of attendance for a course if teacher is part of it
****Add a teacher, selecting which courses to add them to
