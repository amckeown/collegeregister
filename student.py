class Student: # a class of all functions which can be performed on a student
    def __init__ (self, student_params):
        self.first_name = student_params.get('first_name')
        self.last_name = student_params.get('last_name')
        self.student_number = int(student_params.get('student_number'))
        self.student_added = student_params.get('student_added')
        self.student_addedby = student_params.get('student_addedby')
        self.student_removed = student_params.get('course_removed')
        self.student_removedby = student_params.get('course_removedby')

    def get_full_name(self):
        return self.first_name + " " + self.last_name

    def get_student_number(self):
        return self.student_number

    def set_student_added(self, date):
        self.student_added = date

    def set_student_addedby(self, staff_num):
        self.student_addedby = staff_num

    def set_student_removed(self, date):
        self.student_added = date

    def set_student_removedby(self, staff_num):
        self.student_addedby = staff_num
