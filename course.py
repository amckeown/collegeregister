class Course:
    def __init__ (self, course_params):
    #course_number, course_name, course_description, course_start, course_day, course_time, course_duration, course_cost, course_certified, course_isactive):
        self.course_number = int(course_params.get('course_number'))
        self.course_name = course_params.get('course_name')
        self.course_description = course_params.get('course_description')
        self.course_start = course_params.get('course_start')
        self.course_day = course_params.get('course_day')
        self.course_time = course_params.get('course_time')
        self.course_duration = course_params.get('course_duration')
        self.course_cost = course_params.get('course_cost')
        self.course_certified = course_params.get('course_certified')
        self.course_isactive = course_params.get('course_isactive')
        self.course_added = course_params.get('course_added')
        self.course_addedby = course_params.get('course_addedby')
        self.course_removed = course_params.get('course_removed')
        self.course_removedby = course_params.get('course_removedby')

    def get_course_number(self):
        return self.course_number

    def get_course_name(self):
        return self.course_name

    def get_course_description(self):
        return self.course_description

    def get_course_start(self):
        return self.course_start

    def get_course_day(self):
        return self.course_day

    def get_course_time(self):
        return self.course_time

    def get_course_duration(self):
        return self.course_duration

    def get_course_cost(self):
        return self.course_cost

    def get_course_certified(self):
        if self.course_certified:
            return True
        return False

    def get_course_status(self):
        if self.course_isactive:
            return True;
        return False

    def change_course_status(self):
        if self.course_isactive:
            self.course_isactive = False
        else:
            self.course_isactive = True

    def get_course_added(self):
        return self.course_added

    def get_couse_addedby(self):
        return self.course_addedby

    def get_course_removed(self):
        return self.course_removed

    def get_course_removedby(self):
        return self.course_removedby

    def show_course_security_logs(self):
        removed = ""
        if not self.get_course_removed() == None:
             removed = " - Removed: " + str(self.get_course_removed()) + " by " + str(self.get_course_removedby())
        print("\n" + str(self.get_course_number()) + " - " + self.get_course_name() + " - Added: " + str(self.get_course_added()) + " by " + str(self.get_couse_addedby()) + removed)
